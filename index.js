// index.js
const express = require("express");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");

const app = express();
const PORT = process.env.PORT || 3000;

function verifyToken(req, res, next) {
    const token = req.headers["authorization"];
    if (!token) {
        return res.status(401).send("Unauthorized");
    }

    try {
        const decoded = jwt.verify(token, JWT_SECRET);
        req.user = decoded;
        next();
    } catch (error) {
        return res.status(403).send("Invalid token");
    }
}

// Middleware
app.use(bodyParser.json());

// Secret key for JWT
const JWT_SECRET = "@your_jwt_secret_key";

// Middleware for verifying JWT token

// Routes
// Authentication route
const users = [
    { id: 1, username: "user1", password: "password1" },
    { id: 2, username: "user2", password: "password2" },
];
app.post("/api/login", (req, res) => {
    const { username, password } = req.body;
    const user = users.find((u) => u.username === username && u.password === password);
    if (!user) return res.status(401).send("Invalid username or password.");

    const token = jwt.sign({ id: user.id }, JWT_SECRET, { expiresIn: 86400 }); // Expires in 24 hours
    res.status(200).send({ auth: true, token: token });
});

app.use(verifyToken);

let todos = [];

// Create
app.post("/api/todos", (req, res) => {
    const { title, description } = req.body;
    const todo = { id: todos.length + 1, title, description };
    todos.push(todo);
    res.status(201).json(todo);
});

// Read All
app.get("/api/todos", (req, res) => {
    res.json(todos);
});

// Read One
app.get("/api/todos/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const todo = todos.find((todo) => todo.id === id);
    if (!todo) {
        res.status(404).send("Todo not found");
    } else {
        res.json(todo);
    }
});

// Update
app.put("/api/todos/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const { title, description } = req.body;
    const todoIndex = todos.findIndex((todo) => todo.id === id);
    if (todoIndex === -1) {
        res.status(404).send("Todo not found");
    } else {
        todos[todoIndex] = { id, title, description };
        res.json(todos[todoIndex]);
    }
});

// Delete
app.delete("/api/todos/:id", (req, res) => {
    const id = parseInt(req.params.id);
    const todoIndex = todos.findIndex((todo) => todo.id === id);
    if (todoIndex === -1) {
        res.status(404).send("Todo not found");
    } else {
        todos.splice(todoIndex, 1);
        res.status(200).send("Delete success!!");
    }
});

// Start server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
